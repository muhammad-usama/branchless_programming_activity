#include <string.h>
#include <iostream>

#include "benchmark.h"

void __attribute__((noinline)) f(bool cond1, bool cond2, unsigned long value, unsigned long &sum)
{
 
    if(cond1 || cond2)
    {
        sum += value;
    }
}

void BM_5_branch(benchmark::State &state)
{
    srand(1);
    const unsigned int N = state.range(0);
    std::vector<unsigned long> vect1(N);
    std::vector<bool> cond1(N), cond2(N);
    for (size_t index = 0; index < N; ++index)
    {
        vect1[index] = rand();
        cond1[index] = rand() & 0x1;
        cond2[index] = rand() & 0x1;   
    }

    for (auto _ : state)
    {
        unsigned long a1 = 0;
        for (size_t index = 0; index < N; ++index)
        {
            f(cond1[index], cond2[index], vect1[index], a1);
        }
        benchmark::DoNotOptimize(a1);
    }
    state.SetItemsProcessed(N * state.iterations());
}

BENCHMARK(BM_5_branch)->Arg(1 << 22);
BENCHMARK_MAIN();
