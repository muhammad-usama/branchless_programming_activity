include(ExternalProject)

if(NOT TARGET google_benchmark)
ExternalProject_Add(google_benchmark 
    GIT_REPOSITORY https://github.com/google/benchmark
    GIT_TAG        v1.5.2
    CMAKE_ARGS 
              -DCMAKE_INSTALL_PREFIX=${CMAKE_BINARY_DIR} 
              -DBENCHMARK_ENABLE_GTEST_TESTS=OFF
              -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
    PREFIX ${CMAKE_CURRENT_BINARY_DIR}/benchmark
)
endif()

ExternalProject_Get_Property(google_benchmark SOURCE_DIR)
ExternalProject_Get_Property(google_benchmark BINARY_DIR)
set(benchmark_INCLUDE_DIR ${SOURCE_DIR}/include/benchmark)
set(benchmark_BINARY_DIR ${BINARY_DIR}/src)
