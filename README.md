# Install perf 
`sudo apt-get install linux-tools-$(uname -r) linux-tools-generic -y`

# To run with perf
`perf stat ./benchmarks/5a_branch`
